"""errors.py includes NumberError class"""
class NumberError:
    """Raised when the input value is negative number"""
    pass
