import getpass

def login():

    print("Welcome!")
    welcome = input("Do you have an acount? Please type yes or no: ")
    if welcome == "no":
        print("Please register your account!")
        username  = input("Type a username: ")
        password  = getpass.getpass("Type a password: ")
        password_for_confirmation = getpass.getpass("Confirm password: ")
        email = input("Type a email address: ")
        email_for_confirmation = input("Confirm email address: ")
        if (password == password_for_confirmation) and (email == email_for_confirmation):
            with open("users.txt", "a") as file:
                file.write("\n")
                file.write(username + "," + password + "," + email)
                file.close()
                print("Register success!")
            welcome = "yes"
        elif (password != password_for_confirmation) and (email == email_for_confirmation):
            print("Passwords do not match. Try again...")
        elif (password == password_for_confirmation) and (email != email_for_confirmation):
            print("Email address do not match. Try again...")
        else:
            print("Passwords and email address do not match. Try again...")
    if welcome == "yes":
        print("This is login page!")
        username = input("Username: ")
        password = getpass.getpass("Password: ")
        f = open("users.txt", "r")
        for line in f.readlines():
            un,pw,ema = line.strip().split(",", 2)
            if (username == un) and (password == pw):
                print("Login success!")
                return True
            print("Wrong username or password...")
            return False
