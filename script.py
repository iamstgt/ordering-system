from login import login
from item import Item
from errors import NumberError
from order_confirmation_email import send_email

def setup_items():
    item1 = Item("banana",1.0)
    item2 = Item("kiwi",1.5)
    item3 = Item("apple",2.0)
    item4 = Item("pineapple",2.5)
    item5 = Item("grape",3.0)
    items = [item1,item2,item3,item4,item5]

    index = 0 

    for item in items:
        print(str(index) + "." + item.info())
        index += 1
    
    return items

def select_item(items):
    while True:
        try:    
            order = int(input("Type an item number that you want: "))
            if order < 0:
                raise NumberError()
            selected_item = items[order]
            print("Selected item: " + selected_item.name)

            return selected_item
        
        except IndexError:
            print("Oops! That was not a valid number. Try again...")
            continue
        except (ValueError,NameError):
            print("Oops! That was not a number. Try again...")
            continue
        except NumberError:
            print("Oops! That was not a positive number. Try again...")
            continue

def select_quantity(selected_item):
    while True:
        try:
            count = int(input("Type how many items you want (20% discount if you order 3 or more): "))
            if count < 0:
                raise NumberError
            result = selected_item.get_total_price(count)
            print("$" + result + " in total")
            
            return result

        except (ValueError,NameError):
            print("Oops! That was not a number. Try again...")
            continue
        except NumberError:
            print("Oops! That was not a positive number. Try again...")
            continue

def confirm_order():
    while True:
        try:
            confirm = input("Is yor order correct? Please type yes or no: ")
            if confirm == "yes":
                print("Order Received! Please check the email from us!")
                send_email()
                break
            elif confirm == "no":
                print("Order again!")
                items = setup_items()
                selected_item = select_item(items)
                select_quantity(selected_item)
            else:
                print("Oops! That was not valid. Try again...")
                continue
        except ValueError:
            print("Oops! That was not a valid number. Try again...")
            continue

if __name__ == "__main__":
    log = login()
    if log == True:
        items = setup_items()
        selected_item = select_item(items)
        select_quantity(selected_item)
        confirm_order()
    else:
        print('Login Failure...')
        login()