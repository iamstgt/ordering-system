"""item.py includes Item class"""
class Item:
    """This class represents and manipulates name and price coordinates"""
    def __init__(self,name,price):
        """This function allows Item class to initialize the attributes of the class """
        self.name = name
        self.price = price
    def info(self):
        """This function returns item information:name and price"""
        return self.name + ' : $' + str(self.price)
    def get_total_price(self,count):
        """This function returns total price"""
        total_price = self.price * count
        if count >= 3:
            total_price *= 0.8
        return str(round(total_price, 2))
 